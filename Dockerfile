FROM centos:7

RUN yum update -y && yum -y install make gcc-c++ gmp gmp-devel libdb-devel libtool ncurses ncurses-devel ncurses-libs libdbi libdbi-devel libtool-ltdl libtool-ltdl-devel db4 db4-devel && \
    yum clean all && \
    rm -rf /var/cache/yum

RUN curl -L https://sourceforge.net/projects/open-cobol/files/gnu-cobol/2.2/gnucobol-2.2.tar.gz/download -o gnucobol-2.2.tar.gz && \
    tar xzf gnucobol-2.2.tar.gz

RUN cd gnucobol-2.2 && \
    ./configure --prefix=/usr --libdir=/usr/lib64 --with-db --with-pic --with-varseq=1 CFLAGS="-m64" LDFLAGS="-m64" && \
    make && \
#    make check && \
#    make test && \
    make install && \
    ldconfig
